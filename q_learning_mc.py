#!/usr/bin/env python
# coding: utf-8

# ### TD vs. Monte Carlo learning
# 
# The main difference between these two methods is that MC waits until the end of an episode to update the Q table. TD learning updates it after every timestep, giving a better estimation of the state-action value function.

# In[1]:


import gym
import numpy as np


# In[2]:


env = gym.make('MountainCar-v0')


# In[5]:


env.action_space


# In[42]:


obs_bins = round((env.observation_space.high - env.observation_space.low) * np.array([10, 100]) + 1)


# In[ ]:


obs_bins


# In[ ]:


Q = np.zeros((*obs_bins, env.action_space.n))


# In[65]:


tuple(obs_bins)


# In[49]:


obs_bins = ((env.observation_space.high - env.observation_space.low) * np.array([10, 100]) + 1).astype(int)


# In[50]:


obs_bins


# In[51]:


Q = np.zeros((*obs_bins, env.action_space.n))


# In[53]:


(*obs_bins, env.action_space.n)


# In[60]:


np.random.rand()


# In[62]:


env.action_space.sample()


# In[64]:


np.argmax(Q[5,5])


# In[ ]:


env.step()


# In[ ]:


def discretize_state(state):
    state_adj = ((state - env.observation_space.low) * adj_array)
    state_adj = np.round(state_adj, 0).astype(int)
    return state_adj


# In[29]:
epsilon = 0.9


def q_learning(env, max_episodes):
    adj_array = np.array([10, 100])
    
    obs_bins = ((env.observation_space.high - env.observation_space.low) * adj_array + 1)
    obs_bins = np.round(obs_bins, 0).astype(int)
    Q = np.zeros((*obs_bins, env.action_space.n))
    for i in max_episodes:
        
        state = env.reset()
        state = discretize_state(state)
        
        done = False
        while not done:
            # run episode with updates after every timestep
            if np.random.rand() < epsilon:
                action = env.action_space.sample()
            else:
                action = np.argmax(Q[state])
            
            obs, reward, done, info = env.step(action)

            state_2 = discretize_state(obs)

        # update Q table
        update = 5

        Q[state, action] += update

        # before next loop
        state = state_2

        
        

